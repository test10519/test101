<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Assignee</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>4a05322c-0240-47a2-b776-573218de6914</testSuiteGuid>
   <testCaseLink>
      <guid>dacc89c4-e436-400e-9a10-ccf0224e13cb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Filter/Filter - Assignee</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>834a745c-24a6-4b7c-a60c-b000e1c61f12</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/filterAssignee</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>834a745c-24a6-4b7c-a60c-b000e1c61f12</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>assigneeSearch</value>
         <variableId>669559e4-cfc6-45e3-817d-8440124490aa</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>834a745c-24a6-4b7c-a60c-b000e1c61f12</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>expectedAssignee</value>
         <variableId>24eb00d7-5c66-465e-be8c-011fd14bebf0</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
