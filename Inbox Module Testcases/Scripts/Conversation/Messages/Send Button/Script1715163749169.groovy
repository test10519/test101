import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://www.beta.chatdaddy.tech/login')

WebUI.setText(findTestObject('Page_ChatDaddy/input__r1 (27)'), '9205212092')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_ChatDaddy/input_Password_password (45)'), 'bl/ynKqD2d1DDaEllGeQPQ==')

WebUI.sendKeys(findTestObject('Object Repository/Page_ChatDaddy/input_Password_password (45)'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Page_Getting Started/button_Next (30)'))

WebUI.click(findTestObject('Object Repository/Page_Getting Started/button_Close (30)'))

WebUI.click(findTestObject('Object Repository/Page_Getting Started/svg (27)'))

WebUI.click(findTestObject('Object Repository/Page_Getting Started/span_Inbox (28)'))

WebUI.click(findTestObject('Object Repository/Page_(9) Inbox/span_Skip this Short and Sweet Tour (5)'))

WebUI.click(findTestObject('Object Repository/Page_(9) Inbox/span_Jo test (5)'))

WebUI.click(findTestObject('Object Repository/Page_(9) Inbox - Jo test/span_settings (1)'))

WebUI.click(findTestObject('Object Repository/Page_(9) Inbox - Jo test/input_Auto-Suggest Replies_PrivateSwitchBas_1a8e1d (1)'))

WebUI.sendKeys(findTestObject('Object Repository/Page_(9) Inbox - Jo test/input_Auto-Suggest Replies_PrivateSwitchBas_1a8e1d (1)'), 
    Keys.chord(Keys.ESCAPE))

WebUI.click(findTestObject('Object Repository/Page_(9) Inbox - Jo test/span_add (1)'))

WebUI.click(findTestObject('Object Repository/Page_(9) Inbox - Jo test/button_buttons_altButton (1)'))

WebUI.click(findTestObject('Object Repository/Page_(9) Inbox - Jo test/div_'))

WebUI.setText(findTestObject('Object Repository/Page_(9) Inbox - Jo test/input_Reply_r2a'), 'Button 1')

WebUI.sendKeys(findTestObject('Object Repository/Page_(9) Inbox - Jo test/input_Reply_r2a'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Page_(9) Inbox - Jo test/button_Add Button (1)'))





WebUI.setText(findTestObject('Object Repository/Page_(9) Inbox - Jo test/textarea_add_complex-text-area (5)'), 'test button')

WebUI.click(findTestObject('Object Repository/Page_(9) Inbox - Jo test/span_send (2)'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Page_(9) Inbox - Jo test/p_Button 1 (1)'), 0)

WebUI.click(findTestObject('Object Repository/Page_(9) Inbox - Jo test/span_account_circle (5)'))

WebUI.click(findTestObject('Object Repository/Page_(9) Inbox - Jo test/span_Logout (5)'))

WebUI.closeBrowser()

