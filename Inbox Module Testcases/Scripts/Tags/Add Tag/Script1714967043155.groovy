import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://www.beta.chatdaddy.tech/Login')

WebUI.setText(findTestObject('Page_ChatDaddy/input__r1'), '9205212092')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_ChatDaddy/input_Password_password (23)'), 'bl/ynKqD2d1DDaEllGeQPQ==')

WebUI.sendKeys(findTestObject('Object Repository/Page_ChatDaddy/input_Password_password (23)'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Page_Getting Started/button_Next (8)'))

WebUI.click(findTestObject('Object Repository/Page_Getting Started/button_Close (8)'))

WebUI.click(findTestObject('Object Repository/Page_Getting Started/svg (7)'))

WebUI.click(findTestObject('Object Repository/Page_Getting Started/span_Inbox (7)'))

WebUI.click(findTestObject('Object Repository/Page_(11) Inbox/span_Skip this Short and Sweet Tour (2)'))

WebUI.click(findTestObject('Object Repository/Page_(11) Inbox/span_Jo test (2)'))

WebUI.click(findTestObject('Object Repository/Page_(11) Inbox - Jo test/span_Jo test (2)'))

WebUI.click(findTestObject('Object Repository/Page_(11) Inbox - Jo test/span_Add Tag (2)'))

WebUI.setText(findTestObject('Object Repository/Page_(11) Inbox - Jo test/input_search_rds'), 'Add Tag1')

WebUI.click(findTestObject('Object Repository/Page_(11) Inbox - Jo test/span_add (2)'))

WebUI.click(findTestObject('Object Repository/Page_(11) Inbox - Jo test/div_searchcancellabelAdd Tag1'))

WebUI.waitForElementVisible(findTestObject('Object Repository/Page_(11) Inbox - Jo test/div_searchcancellabelAdd Tag1'), 
    10)

WebUI.delay(10 // adds a 1-second delay to ensure the page/elements are ready
    )

WebUI.verifyTextPresent('Add Tag1', false)

WebUI.click(findTestObject('Object Repository/Page_(11) Inbox - Jo test/span_account_circle (2)'))

WebUI.click(findTestObject('Object Repository/Page_(11) Inbox - Jo test/span_Logout (2)'))

WebUI.closeBrowser()

