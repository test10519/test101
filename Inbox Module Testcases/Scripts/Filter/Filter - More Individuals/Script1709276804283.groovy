import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://www.beta.chatdaddy.tech/Login')

WebUI.setText(findTestObject('Page_ChatDaddy/input__r0'), '9261505795')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_ChatDaddy/input_Password_password (9)'), 'k3vh9FOVV1+Tg1rlITHUaA==')

WebUI.sendKeys(findTestObject('Object Repository/Page_ChatDaddy/input_Password_password (9)'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Page_Home/div_Home_MuiGrid-root css-1d2hfx (9)'))

WebUI.click(findTestObject('Object Repository/Page_Home/span_Inbox (9)'))

WebUI.click(findTestObject('Object Repository/Page_(323) Inbox/span_Skip this Short and Sweet Tour (1)'))

WebUI.click(findTestObject('Object Repository/Page_(323) Inbox/button_Morearrow_drop_down (1)'))

WebUI.click(findTestObject('Object Repository/Page_(323) Inbox/input_Show individual chats_PrivateSwitchBa_344b21'))

WebUI.sendKeys(findTestObject('Object Repository/Page_(323) Inbox/input_Show individual chats_PrivateSwitchBa_344b21'), 
    Keys.chord(Keys.ESCAPE))

WebUI.setText(findTestObject('Object Repository/Page_Inbox/input_search_r8 (1)'), 'ivan')

WebUI.sendKeys(findTestObject('Object Repository/Page_Inbox/input_search_r8 (1)'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Page_Inbox/div_Ivan23h'))

WebUI.rightClick(findTestObject('Object Repository/Page_Inbox - Ivan/div_Ivan23h'))

WebUI.click(findTestObject('Object Repository/Page_Inbox - Ivan/li_portraitContact Info (3)'))

WebUI.verifyTextNotPresent('Group Details', false)

WebUI.closeBrowser()

