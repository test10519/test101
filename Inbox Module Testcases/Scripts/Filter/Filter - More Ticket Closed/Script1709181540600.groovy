import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://www.beta.chatdaddy.tech/Login')

WebUI.setText(findTestObject('Page_ChatDaddy/input__r0'), '9205212092')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_ChatDaddy/input_Password_password (3)'), 'bl/ynKqD2d1DDaEllGeQPQ==')

WebUI.sendKeys(findTestObject('Object Repository/Page_ChatDaddy/input_Password_password (3)'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Page_Home/div_Home_MuiGrid-root css-1d2hfx (3)'))

WebUI.click(findTestObject('Object Repository/Page_Home/span_Inbox (3)'))

WebUI.click(findTestObject('Object Repository/Page_(312) Inbox/span_Skip this Short and Sweet Tour'))

WebUI.setText(findTestObject('Object Repository/Page_Inbox/input_search_r8'), 'ivan')

WebUI.sendKeys(findTestObject('Object Repository/Page_Inbox/input_search_r8'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Page_Inbox/div_Ivan2h'))

WebUI.click(findTestObject('Object Repository/Page_Inbox - Ivan/span_done'))

WebUI.click(findTestObject('Object Repository/Page_Inbox - Ivan/span_cancel'))

WebUI.click(findTestObject('Object Repository/Page_(312) Inbox - Ivan/button_Morearrow_drop_down'))

WebUI.click(findTestObject('Object Repository/Page_(312) Inbox - Ivan/input_Show contacts with resolved tasks_Pri_2cff0e'))

WebUI.sendKeys(findTestObject('Object Repository/Page_(312) Inbox - Ivan/input_Show contacts with resolved tasks_Pri_2cff0e'), 
    Keys.chord(Keys.ESCAPE))

WebUI.click(findTestObject('Object Repository/Page_Inbox - Ivan/span_Ivan (1)'))

WebUI.verifyImagePresent(findTestObject('Page_Inbox - Ivan/span_done'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Page_Inbox - Ivan/span_account_circle (1)'))

WebUI.click(findTestObject('Object Repository/Page_Inbox - Ivan/li_logoutLogout'))

WebUI.closeBrowser()

