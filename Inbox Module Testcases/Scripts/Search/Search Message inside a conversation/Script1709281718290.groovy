import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://www.beta.chatdaddy.tech/Login')

WebUI.setText(findTestObject('Object Repository/Page_ChatDaddy/input__r1 (15)'), '9205212092')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_ChatDaddy/input_Password_password (33)'), 'bl/ynKqD2d1DDaEllGeQPQ==')

WebUI.sendKeys(findTestObject('Object Repository/Page_ChatDaddy/input_Password_password (33)'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Page_Getting Started/button_Next (18)'))

WebUI.click(findTestObject('Object Repository/Page_Getting Started/button_Close (18)'))

WebUI.click(findTestObject('Object Repository/Page_Getting Started/svg (16)'))

WebUI.click(findTestObject('Object Repository/Page_Getting Started/span_Inbox (16)'))

WebUI.click(findTestObject('Object Repository/Page_(11) Inbox/span_Skip this Short and Sweet Tour (8)'))

WebUI.setText(findTestObject('Page_Inbox/input_search_r18 (3)'), 'ivan')

WebUI.sendKeys(findTestObject('Page_Inbox/input_search_r18 (3)'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Page_Inbox/span_Ivan (11)'))

WebUI.click(findTestObject('Object Repository/Page_Inbox - Ivan/span_search (2)'))

WebUI.setText(findTestObject('Object Repository/Page_Inbox - Ivan/input_Drop Files Here_r22'), 'test')

WebUI.sendKeys(findTestObject('Object Repository/Page_Inbox - Ivan/input_Drop Files Here_r22'), Keys.chord(Keys.ENTER))

WebUI.verifyElementText(findTestObject('Object Repository/Page_Inbox - Ivan/div_test (2)'), 'test')

WebUI.click(findTestObject('Object Repository/Page_Inbox - Ivan/span_account_circle (6)'))

WebUI.click(findTestObject('Object Repository/Page_Inbox - Ivan/span_Logout (5)'))

WebUI.closeBrowser()

