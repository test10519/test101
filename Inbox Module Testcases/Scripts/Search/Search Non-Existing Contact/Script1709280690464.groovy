import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://www.beta.chatdaddy.tech/Login')

WebUI.setText(findTestObject('Page_ChatDaddy/input__r1'), '9205212092')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_ChatDaddy/input_Password_password (34)'), 'bl/ynKqD2d1DDaEllGeQPQ==')

WebUI.sendKeys(findTestObject('Object Repository/Page_ChatDaddy/input_Password_password (34)'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Page_Getting Started/button_Next (19)'))

WebUI.click(findTestObject('Object Repository/Page_Getting Started/button_Close (19)'))

WebUI.click(findTestObject('Object Repository/Page_Getting Started/svg (17)'))

WebUI.click(findTestObject('Object Repository/Page_Getting Started/span_Inbox (17)'))

WebUI.click(findTestObject('Object Repository/Page_(10) Inbox/span_Skip this Short and Sweet Tour'))

WebUI.delay(10)

WebUI.setText(findTestObject('Page_Inbox/input_search_r18 (3)'), 'uadbjasdamnduiahsdnasdnjasdaskd')

WebUI.sendKeys(findTestObject('Page_Inbox/input_search_r18 (3)'), Keys.chord(Keys.ENTER))

WebUI.verifyElementText(findTestObject('Object Repository/Page_Inbox/p_Sorry We couldnt find the chat you searched for (1)'), 
    'Sorry! We couldn’t find the chat you searched for.')

WebUI.click(findTestObject('Object Repository/Page_Inbox/span_account_circle (2)'))

WebUI.click(findTestObject('Object Repository/Page_Inbox/span_Logout (2)'))

WebUI.closeBrowser()

