<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Skip this Short and Sweet Tour (21)</name>
   <tag></tag>
   <elementGuidId>a54bbfb4-46b8-4af3-9da4-242509a0b311</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='react-joyride-step-0']/div/div/div/div[2]/button[2]/span</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'MuiTypography-root MuiTypography-smallSemiBold css-1ocob9' and (text() = 'Skip this Short and Sweet Tour' or . = 'Skip this Short and Sweet Tour')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.MuiTypography-root.MuiTypography-smallSemiBold.css-1ocob9</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>5d549c3b-b329-4eb0-aaba-12d810654007</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiTypography-root MuiTypography-smallSemiBold css-1ocob9</value>
      <webElementGuid>89cec934-8db0-473a-b3bb-d541da07451d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Skip this Short and Sweet Tour</value>
      <webElementGuid>0118316c-03c5-44b8-a212-742aa555bfe4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;react-joyride-step-0&quot;)/div[@class=&quot;__floater __floater__open&quot;]/div[@class=&quot;__floater__body&quot;]/div[@class=&quot;MuiGrid-root css-grxqdy&quot;]/div[@class=&quot;MuiGrid-root css-b486k7&quot;]/button[@class=&quot;MuiButtonBase-root MuiButton-root MuiButton-text MuiButton-textPrimary MuiButton-sizeMedium MuiButton-textSizeMedium MuiButton-colorPrimary MuiButton-root MuiButton-text MuiButton-textPrimary MuiButton-sizeMedium MuiButton-textSizeMedium MuiButton-colorPrimary css-iotrky&quot;]/span[@class=&quot;MuiTypography-root MuiTypography-smallSemiBold css-1ocob9&quot;]</value>
      <webElementGuid>4b4500cc-8a00-4edf-87c3-5ffee872add4</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='react-joyride-step-0']/div/div/div/div[2]/button[2]/span</value>
      <webElementGuid>83097ef7-2389-4f9f-80ff-55b1f24e5318</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Take the Tour'])[1]/following::span[2]</value>
      <webElementGuid>b08c6b62-8012-4430-9e43-9bf197a5b264</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='It’s going to take just a few seconds.'])[1]/following::span[3]</value>
      <webElementGuid>3f734428-ef35-4249-822e-a919b3663df7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Skip this Short and Sweet Tour']/parent::*</value>
      <webElementGuid>f59ec999-9741-4932-a651-20b2eeb08848</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/div/div/div[2]/button[2]/span</value>
      <webElementGuid>39e7f30c-ef3b-475b-806c-09ae40772871</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Skip this Short and Sweet Tour' or . = 'Skip this Short and Sweet Tour')]</value>
      <webElementGuid>93736086-f044-4098-a3ab-d66e6d00501c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
