<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Skip this Short and Sweet Tour (20) (2) (2) (2)</name>
   <tag></tag>
   <elementGuidId>2222c63e-7897-4f67-989f-8a679997b16c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='react-joyride-step-0']/div/div/div/div[2]/button[2]/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.MuiTypography-root.MuiTypography-smallSemiBold.css-1ocob9</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>0c6686a9-b3e1-4845-89f3-869f7534b112</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiTypography-root MuiTypography-smallSemiBold css-1ocob9</value>
      <webElementGuid>92f555a1-410d-46f5-9f93-03035a4e5d97</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Skip this Short and Sweet Tour</value>
      <webElementGuid>83e61884-0f07-431e-8e16-66478839065b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;react-joyride-step-0&quot;)/div[@class=&quot;__floater __floater__open&quot;]/div[@class=&quot;__floater__body&quot;]/div[@class=&quot;MuiGrid-root css-grxqdy&quot;]/div[@class=&quot;MuiGrid-root css-b486k7&quot;]/button[@class=&quot;MuiButtonBase-root MuiButton-root MuiButton-text MuiButton-textPrimary MuiButton-sizeMedium MuiButton-textSizeMedium MuiButton-colorPrimary MuiButton-root MuiButton-text MuiButton-textPrimary MuiButton-sizeMedium MuiButton-textSizeMedium MuiButton-colorPrimary css-iotrky&quot;]/span[@class=&quot;MuiTypography-root MuiTypography-smallSemiBold css-1ocob9&quot;]</value>
      <webElementGuid>e915aca7-8b5a-4e2a-98d1-6b498a1740ae</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='react-joyride-step-0']/div/div/div/div[2]/button[2]/span</value>
      <webElementGuid>78b8f7e4-7820-436d-87f0-466ad1074165</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Take the Tour'])[1]/following::span[2]</value>
      <webElementGuid>d223e28b-fb25-41f7-82e7-0105701aed94</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='It’s going to take just a few seconds.'])[1]/following::span[3]</value>
      <webElementGuid>1790fc90-8d26-44fd-bebe-c8eb1bd130a4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Skip this Short and Sweet Tour']/parent::*</value>
      <webElementGuid>6089428b-746a-4881-9a58-257c8e471e42</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/div/div/div[2]/button[2]/span</value>
      <webElementGuid>8fa656ac-39e0-48db-a3c1-94fb77d791ec</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Skip this Short and Sweet Tour' or . = 'Skip this Short and Sweet Tour')]</value>
      <webElementGuid>0a2f9bce-feec-4d1e-a0cf-ac0adfd09bd1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
