<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_601113219100</name>
   <tag></tag>
   <elementGuidId>c596ac7e-7bdb-409c-91d8-d491dca9ff76</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='inbox-header']/div[2]/div/div/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.MuiTypography-root.MuiTypography-xSmallRegular.css-17psejy</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>8a1f2295-66be-4d98-9167-5ee1694f2eb9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiTypography-root MuiTypography-xSmallRegular css-17psejy</value>
      <webElementGuid>87d7d415-26fc-4f64-adf8-fa1de1823b1b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>601113219100</value>
      <webElementGuid>bc79f426-5426-40fb-857b-50de3e67e46a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>601113219100</value>
      <webElementGuid>3187aca4-10f2-4ce9-80f4-0ff087c18ff5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;inbox-header&quot;)/div[@class=&quot;MuiGrid-root css-3oh7ub&quot;]/div[@class=&quot;MuiGrid-root css-13jlrw4&quot;]/div[@class=&quot;MuiGrid-root css-jz608q&quot;]/span[@class=&quot;MuiTypography-root MuiTypography-xSmallRegular css-17psejy&quot;]</value>
      <webElementGuid>7eba90d8-e04b-4ded-b608-999ac6182940</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='inbox-header']/div[2]/div/div/span</value>
      <webElementGuid>8e119e78-e475-429d-bf1b-13c5c5343de6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='expand_more'])[11]/following::span[2]</value>
      <webElementGuid>2f1ba6a7-4872-40a1-964f-4e3e29988617</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='done'])[5]/preceding::span[1]</value>
      <webElementGuid>a2acc588-e1a9-48fb-93de-30e16726ca7b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='search'])[2]/preceding::span[3]</value>
      <webElementGuid>264e9e65-e265-4e96-a8e7-3287d7ff38ba</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div[2]/div/div/span</value>
      <webElementGuid>10d6bdba-ed51-4b31-84a2-00bc754778b8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[@title = '601113219100' and (text() = '601113219100' or . = '601113219100')]</value>
      <webElementGuid>0f6b11a1-366a-47f3-8433-767113da9fa0</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
