<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Skip this Short and Sweet Tour (1)</name>
   <tag></tag>
   <elementGuidId>75171bce-3509-48a7-bfc0-f2741a72c45c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='react-joyride-step-0']/div/div/div/div[2]/button[2]/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.MuiTypography-root.MuiTypography-smallSemiBold.css-1ocob9</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>108c9d23-27c1-498f-8c3b-cbe80ba3d3a5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiTypography-root MuiTypography-smallSemiBold css-1ocob9</value>
      <webElementGuid>f395b9bc-a723-40b6-a898-236aa10d06b5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Skip this Short and Sweet Tour</value>
      <webElementGuid>7e1cfa33-820a-4385-a800-eb2909fad362</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;react-joyride-step-0&quot;)/div[@class=&quot;__floater __floater__open&quot;]/div[@class=&quot;__floater__body&quot;]/div[@class=&quot;MuiGrid-root css-grxqdy&quot;]/div[@class=&quot;MuiGrid-root css-b486k7&quot;]/button[@class=&quot;MuiButtonBase-root MuiButton-root MuiButton-text MuiButton-textPrimary MuiButton-sizeMedium MuiButton-textSizeMedium MuiButton-root MuiButton-text MuiButton-textPrimary MuiButton-sizeMedium MuiButton-textSizeMedium css-1lih7b1&quot;]/span[@class=&quot;MuiTypography-root MuiTypography-smallSemiBold css-1ocob9&quot;]</value>
      <webElementGuid>07600adc-31e2-4cc8-b240-62374952ead0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='react-joyride-step-0']/div/div/div/div[2]/button[2]/span</value>
      <webElementGuid>902324f1-8424-4e17-b48e-c1b51e98540b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Take the Tour'])[1]/following::span[2]</value>
      <webElementGuid>3ae3e166-5bf3-4963-b000-26b5bc8757d1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='It’s going to take just a few seconds.'])[1]/following::span[3]</value>
      <webElementGuid>9f5c9fb3-c4b8-4002-bab3-44402e5d2b22</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Skip this Short and Sweet Tour']/parent::*</value>
      <webElementGuid>566054b6-7718-4e46-b68c-26f1a0cd050b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/div/div/div[2]/button[2]/span</value>
      <webElementGuid>69e4c9fb-d230-4286-85cd-074c460022ce</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Skip this Short and Sweet Tour' or . = 'Skip this Short and Sweet Tour')]</value>
      <webElementGuid>28f750e9-dabf-4de9-a052-a567e08ad041</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
