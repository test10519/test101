<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_add</name>
   <tag></tag>
   <elementGuidId>a4fba3f2-e19b-48f6-a691-961525c4eb6c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='​'])[16]/following::span[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.MuiButton-icon.MuiButton-endIcon.MuiButton-iconSizeMedium.css-pt151d > span.material-symbols-outlined.MuiBox-root.css-14d0z92</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>f2116e48-ce53-4967-8bbf-cd1b2fab94c9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>material-symbols-outlined MuiBox-root css-14d0z92</value>
      <webElementGuid>8e923837-10ce-4b7e-96d6-d10fd57c173b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>add</value>
      <webElementGuid>d4b3c3ef-ba77-4053-91d7-9ce288610181</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;hydrated&quot;]/body[1]/div[@class=&quot;MuiPopover-root MuiModal-root css-jp7szo&quot;]/div[@class=&quot;MuiPaper-root MuiPaper-25 MuiPaper-rounded MuiPopover-paper css-1qk7w6v&quot;]/div[2]/div[2]/div[1]/button[@class=&quot;MuiButtonBase-root MuiButton-root MuiButton-text MuiButton-textPrimary MuiButton-sizeMedium MuiButton-textSizeMedium MuiButton-colorPrimary MuiButton-root MuiButton-text MuiButton-textPrimary MuiButton-sizeMedium MuiButton-textSizeMedium MuiButton-colorPrimary css-14vzcio&quot;]/span[@class=&quot;MuiButton-icon MuiButton-endIcon MuiButton-iconSizeMedium css-pt151d&quot;]/span[@class=&quot;material-symbols-outlined MuiBox-root css-14d0z92&quot;]</value>
      <webElementGuid>5bc922b6-91dd-4d4d-a761-0c640cce4191</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='​'])[16]/following::span[2]</value>
      <webElementGuid>165f3891-2f74-498f-9693-e803aca8abde</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div/button/span/span</value>
      <webElementGuid>bcc19e8c-e440-4445-ab95-9117efe4108c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'add' or . = 'add')]</value>
      <webElementGuid>9a426dc3-0982-49c8-a9e3-310e9a5146ed</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
