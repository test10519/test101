<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Jo test</name>
   <tag></tag>
   <elementGuidId>88e6e589-f06b-40d8-96f7-31c4e882dcce</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='inbox-header']/div/div/div/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.MuiTypography-root.MuiTypography-mediumSemiBold.css-1t8dp5s</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>384677bf-2f65-446e-a59e-5dc911c3e54c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiTypography-root MuiTypography-mediumSemiBold css-1t8dp5s</value>
      <webElementGuid>ea602864-ba8d-434b-817e-84dbf9e27fff</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>contenteditable</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>14f6ee54-1d90-4ed9-90dd-a5b8ff7ee120</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Jo test</value>
      <webElementGuid>9ee5ab31-a544-4e5e-9991-eb261fb81116</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;inbox-header&quot;)/div[@class=&quot;MuiGrid-root css-u2a8x6&quot;]/div[@class=&quot;MuiGrid-root tour-contact-info css-7bcfnq&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-wrap-xs-nowrap css-jznt39&quot;]/span[@class=&quot;MuiTypography-root MuiTypography-mediumSemiBold css-1t8dp5s&quot;]</value>
      <webElementGuid>5763b857-a042-4b7d-a83a-52262013c8a2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='inbox-header']/div/div/div/span</value>
      <webElementGuid>5855c8de-56ee-4b9a-9ca4-5bfddffc9c3c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='account_circle'])[10]/following::span[1]</value>
      <webElementGuid>f5fe1783-ba9e-4a88-b25e-c0a73c9feec3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='expand_more'])[10]/following::span[4]</value>
      <webElementGuid>0b1e3987-f2be-4f74-a340-592057e66ed0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='edit'])[1]/preceding::span[1]</value>
      <webElementGuid>18203f05-a089-42c7-8c7d-e1bbec97328c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='phone'])[1]/preceding::span[3]</value>
      <webElementGuid>c79032cc-cd8a-40bb-9ffb-0413e56bdeb8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div[3]/div/div/div/div/span</value>
      <webElementGuid>74a05cfd-b4f4-466f-b656-9c19e037ec56</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Jo test' or . = 'Jo test')]</value>
      <webElementGuid>29b3c7ee-e693-41ef-83db-4b3cf48ddf20</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
