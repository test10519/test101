<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Add Tag</name>
   <tag></tag>
   <elementGuidId>441c183b-1dbc-47bd-863c-d953268ffdcd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div[3]/div[3]/div/div[3]/div[2]/div[2]/div/div/button/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.MuiTypography-root.MuiTypography-smallMedium.css-13dgrh4</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>dfbd48a2-9640-41b1-9f61-093ffd6c3196</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiTypography-root MuiTypography-smallMedium css-13dgrh4</value>
      <webElementGuid>bbaa28cb-f0c7-4e79-beae-cc18fa11215f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>+ Add Tag</value>
      <webElementGuid>63bae479-1aad-4685-8efc-3332c53da932</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column MuiGrid-wrap-xs-nowrap css-5u791s&quot;]/div[@class=&quot;MuiGrid-root css-qrdc5g&quot;]/div[@class=&quot;MuiGrid-root css-1r3ro9p&quot;]/div[@class=&quot;MuiBox-root css-13tphjr&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-wrap-xs-nowrap css-1112fw4&quot;]/div[@class=&quot;MuiGrid-root css-1fghw63&quot;]/div[@class=&quot;MuiGrid-root css-1eiz13t&quot;]/div[@class=&quot;MuiGrid-root css-3q639z&quot;]/button[@class=&quot;MuiButtonBase-root MuiButton-root MuiButton-text MuiButton-textPrimary MuiButton-sizeMedium MuiButton-textSizeMedium MuiButton-colorPrimary MuiButton-root MuiButton-text MuiButton-textPrimary MuiButton-sizeMedium MuiButton-textSizeMedium MuiButton-colorPrimary css-9o3257&quot;]/span[@class=&quot;MuiTypography-root MuiTypography-smallMedium css-13dgrh4&quot;]</value>
      <webElementGuid>e2822251-58ba-44a3-ada3-4dd73e0fefdc</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div[3]/div[3]/div/div[3]/div[2]/div[2]/div/div/button/span</value>
      <webElementGuid>b9bc8d68-19d4-4d03-8e48-98cc6789d8e9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='close'])[1]/following::span[2]</value>
      <webElementGuid>ff24814e-d967-48e9-89cf-ee2afd5a5e72</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Drop Files Here'])[1]/following::span[3]</value>
      <webElementGuid>817b73af-4586-4c70-9bf1-781bdba6319e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='VIP'])[2]/preceding::span[1]</value>
      <webElementGuid>efc17b25-cfc3-4814-a0f4-eab2eba72104</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='New Tag'])[2]/preceding::span[2]</value>
      <webElementGuid>ee3738cf-e35a-4c74-83e9-8d4527269aff</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='+ Add Tag']/parent::*</value>
      <webElementGuid>356f2756-8453-4b7a-a307-f41bc5125da1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div/div/button/span</value>
      <webElementGuid>079de1f5-22cb-4d07-a0a9-7639c6a1fc4b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = '+ Add Tag' or . = '+ Add Tag')]</value>
      <webElementGuid>0d7ec03c-ff16-479d-8419-8b242f9720ac</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
