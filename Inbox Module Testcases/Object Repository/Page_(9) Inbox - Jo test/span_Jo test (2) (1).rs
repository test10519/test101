<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Jo test (2) (1)</name>
   <tag></tag>
   <elementGuidId>7c517b9d-634e-41ec-b4d5-7f0a657b9f29</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='inbox-header']/div/div/div/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.MuiTypography-root.MuiTypography-mediumSemiBold.css-1t8dp5s</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>180d291c-0394-40a0-9bfe-456e92ddc0cc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiTypography-root MuiTypography-mediumSemiBold css-1t8dp5s</value>
      <webElementGuid>f224d94f-fb01-454f-ba88-86d5c6eb574e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>contenteditable</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>e2b5cd42-991f-416a-b003-a44dcd7e55f8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Jo test</value>
      <webElementGuid>382965ab-d254-4485-83c7-69c8b4e424cf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;inbox-header&quot;)/div[@class=&quot;MuiGrid-root css-u2a8x6&quot;]/div[@class=&quot;MuiGrid-root tour-contact-info css-7bcfnq&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-wrap-xs-nowrap css-jznt39&quot;]/span[@class=&quot;MuiTypography-root MuiTypography-mediumSemiBold css-1t8dp5s&quot;]</value>
      <webElementGuid>529e8d65-b7cd-4cb9-afe5-bdd821ba24d4</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='inbox-header']/div/div/div/span</value>
      <webElementGuid>7e0ad85b-5f25-4e5a-a305-4a3fff0eba3e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='account_circle'])[8]/following::span[1]</value>
      <webElementGuid>bb7e0982-2de6-45e3-8139-ed183f736eaa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='expand_more'])[10]/following::span[4]</value>
      <webElementGuid>0c4ede47-9a65-41e3-a14d-ecf1e892ab4a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='edit'])[1]/preceding::span[1]</value>
      <webElementGuid>82e00e41-2101-404e-ae4c-4e1714fd7edf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='phone'])[1]/preceding::span[3]</value>
      <webElementGuid>f78dba4a-d3be-4e94-9213-9019baebb765</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div[3]/div/div/div/div/span</value>
      <webElementGuid>9d2db71f-853d-42f3-8c5c-855a5b49613d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Jo test' or . = 'Jo test')]</value>
      <webElementGuid>0ad86320-5754-496e-a6d3-049dfcf9b22c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
