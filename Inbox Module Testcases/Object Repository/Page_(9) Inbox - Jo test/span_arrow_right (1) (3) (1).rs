<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_arrow_right (1) (3) (1)</name>
   <tag></tag>
   <elementGuidId>b19207d2-dacc-48f5-abe4-cff71bbb988f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div[3]/div[3]/div/div[3]/div[2]/div[2]/div/div[4]/div/span/div/div/div/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.material-symbols-rounded.MuiBox-root.css-159niyq</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>e01a438f-5425-4475-bc27-623eddff1f71</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>material-symbols-rounded MuiBox-root css-159niyq</value>
      <webElementGuid>8f777f94-5eda-4fc3-8263-adc914d8074f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>arrow_right</value>
      <webElementGuid>b18c9e34-9fb1-4029-a4a0-de20f539f37a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column MuiGrid-wrap-xs-nowrap css-5u791s&quot;]/div[@class=&quot;MuiGrid-root css-qrdc5g&quot;]/div[@class=&quot;MuiGrid-root css-1r3ro9p&quot;]/div[@class=&quot;MuiBox-root css-13tphjr&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-wrap-xs-nowrap css-1112fw4&quot;]/div[@class=&quot;MuiGrid-root css-1fghw63&quot;]/div[@class=&quot;MuiGrid-root css-1eiz13t&quot;]/div[@class=&quot;MuiGrid-root css-7j22ch&quot;]/div[@class=&quot;MuiGrid-root css-1c8gsuk&quot;]/span[@class=&quot;MuiTypography-root MuiTypography-baseMedium css-1oz3psz&quot;]/div[@class=&quot;MuiCollapse-root MuiCollapse-horizontal MuiCollapse-entered css-1cijpyl&quot;]/div[@class=&quot;MuiCollapse-wrapper MuiCollapse-horizontal css-164swfl&quot;]/div[@class=&quot;MuiCollapse-wrapperInner MuiCollapse-horizontal css-1mziwkx&quot;]/span[@class=&quot;material-symbols-rounded MuiBox-root css-159niyq&quot;]</value>
      <webElementGuid>5c6a11f2-535b-4d97-885f-5a6fe704e177</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div[3]/div[3]/div/div[3]/div[2]/div[2]/div/div[4]/div/span/div/div/div/span</value>
      <webElementGuid>af6171be-0505-4942-be65-48bc9abc3e07</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='May 9, 2024 at 3:32 PM'])[2]/following::span[3]</value>
      <webElementGuid>3fc1e0b2-63ea-4676-802f-8ebb325cda69</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='grid_view'])[1]/preceding::span[1]</value>
      <webElementGuid>8a7ea733-5a6b-4770-876b-523a1e41060a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='add'])[4]/preceding::span[2]</value>
      <webElementGuid>51568154-3e58-4be4-9252-c2964427a360</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/span/div/div/div/span</value>
      <webElementGuid>95ef7ef4-1d72-4a2b-a845-f76317796d66</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'arrow_right' or . = 'arrow_right')]</value>
      <webElementGuid>381f7b01-d2e6-4804-8c9a-3061e4f84be6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
