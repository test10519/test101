<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_Button 1</name>
   <tag></tag>
   <elementGuidId>48a31c55-f24a-4d7d-b57d-528efc1986ee</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='3EB0CDFEDC35BDF6BFC|1-content']/div[2]/div/p</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>p.MuiTypography-root.MuiTypography-body1.css-egrk5c</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>172dc1c9-f365-4edf-a5ea-fd758e8829da</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiTypography-root MuiTypography-body1 css-egrk5c</value>
      <webElementGuid>6bf5f74e-7ca0-43d0-9fc2-50ab4eda7094</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Button 1</value>
      <webElementGuid>a8ecae8f-5232-4250-9261-ebe6b33ce5dd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;3EB0CDFEDC35BDF6BFC|1-content&quot;)/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column css-rg2bq0&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-item css-12yvitw&quot;]/p[@class=&quot;MuiTypography-root MuiTypography-body1 css-egrk5c&quot;]</value>
      <webElementGuid>b49eda86-ddf5-4b1f-88ac-c8b111b8d522</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='3EB0CDFEDC35BDF6BFC|1-content']/div[2]/div/p</value>
      <webElementGuid>2ede9c4e-e01a-4047-8c19-bd49bd74c517</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='test button'])[2]/following::p[1]</value>
      <webElementGuid>d278939f-4d89-4d1f-9934-da0f79337947</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='done_all'])[22]/following::p[1]</value>
      <webElementGuid>8ec342f3-2588-4f5c-8561-403e219616fe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jo Arao'])[14]/preceding::p[4]</value>
      <webElementGuid>0f6cefb1-8fe2-4911-a49f-f68d4f12a080</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='May 8, 2024 at 6:21 PM'])[1]/preceding::p[4]</value>
      <webElementGuid>d013c210-8b12-46d6-a0f7-d154fb6642ee</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Button 1']/parent::*</value>
      <webElementGuid>972aa1f6-407c-47df-90e2-8329d8a38121</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/p</value>
      <webElementGuid>381cdf7d-0908-4a4a-81e8-4ac9e551a583</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'Button 1' or . = 'Button 1')]</value>
      <webElementGuid>4fbb5cbc-3fa8-4f18-b4da-97f74e480155</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
