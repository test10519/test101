<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Logout (12) (2)</name>
   <tag></tag>
   <elementGuidId>5ff475fb-aa66-48c6-a209-c397021fe712</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='logout'])[1]/following::span[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.MuiTypography-root.MuiTypography-smallRegular.css-1d2s0c2</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>c797bf68-39fd-4f82-885e-915a3a05bf11</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiTypography-root MuiTypography-smallRegular css-1d2s0c2</value>
      <webElementGuid>08fb67f0-1e86-4aff-9a03-fa11aee1bb8e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Logout</value>
      <webElementGuid>f08d5a9c-d3fe-44d0-9354-67a6a319bbf2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;hydrated&quot;]/body[1]/div[@class=&quot;MuiPopover-root MuiModal-root css-jp7szo&quot;]/div[@class=&quot;MuiPaper-root MuiPaper-elevation MuiPaper-rounded MuiPaper-elevation8 MuiPopover-paper css-6u8kux&quot;]/div[@class=&quot;MuiBox-root css-1r99qxv&quot;]/li[@class=&quot;MuiButtonBase-root MuiMenuItem-root MuiMenuItem-gutters MuiMenuItem-root MuiMenuItem-gutters css-105266g&quot;]/span[@class=&quot;MuiTypography-root MuiTypography-smallRegular css-1d2s0c2&quot;]</value>
      <webElementGuid>2e0a7d20-1654-4c40-9014-5f9f000acd56</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='logout'])[1]/following::span[1]</value>
      <webElementGuid>17a048e8-bb47-4c19-935d-4c567f879bf2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Collect Locale URLS'])[1]/following::span[3]</value>
      <webElementGuid>eef44bdb-7d1e-4671-92bf-168ca2949664</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Logout']/parent::*</value>
      <webElementGuid>21671f39-2f65-4e1b-935d-4f8bbf420d60</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[7]/span[2]</value>
      <webElementGuid>9b0e5d8f-488b-4ef2-a46d-82cb6d108e8a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Logout' or . = 'Logout')]</value>
      <webElementGuid>5122ad47-4dfa-4c10-84a2-55ec26d3c688</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
