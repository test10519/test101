<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Payment Reminder NotificationHello Than_97f31a</name>
   <tag></tag>
   <elementGuidId>00e63839-e07b-4467-90be-363f3967b92c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='3EB0CDFEDB46134E610|1-content']/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>f419d132-01bb-4a8a-b1d7-bc1bad4a6b15</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>contenteditable</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>9d72f7a3-6c04-441f-8b44-91f49f755872</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>🔥💲*Payment Reminder Notification*

Hello! 

Thank you for your purchase 😀

=========

 you have an outstanding order for:

💰*Total: HKD 10*

=========

We'd like to remind you that you can make your payment through scanning the QR Code or the link/s below.

 🙆‍♂️🙇‍♀️Please don't hesitate to reach out to us if you are facing any issues with the payments and don't forget to send us the proof of payment. Thanks!🙇‍♂️🙏</value>
      <webElementGuid>705af621-6e98-4e55-827b-b1e757a7ea58</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;3EB0CDFEDB46134E610|1-content&quot;)/div[1]</value>
      <webElementGuid>fea5eb32-cd98-4618-9774-8628d0600d83</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='3EB0CDFEDB46134E610|1-content']/div</value>
      <webElementGuid>8a50df8f-2629-4a48-b53a-35d7fab35bc8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='done_all'])[20]/following::div[4]</value>
      <webElementGuid>3f8da47a-2bc7-47d8-b662-89134f293633</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='May 9, 2024 at 11:03 AM'])[1]/following::div[4]</value>
      <webElementGuid>f79bcf10-4e76-4037-9501-2b1d42ce6c59</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[16]/div/div/div</value>
      <webElementGuid>a8070022-3098-40d7-8150-f4ec28dd46d0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = concat(&quot;🔥💲*Payment Reminder Notification*

Hello! 

Thank you for your purchase 😀

=========

 you have an outstanding order for:

💰*Total: HKD 10*

=========

We&quot; , &quot;'&quot; , &quot;d like to remind you that you can make your payment through scanning the QR Code or the link/s below.

 🙆‍♂️🙇‍♀️Please don&quot; , &quot;'&quot; , &quot;t hesitate to reach out to us if you are facing any issues with the payments and don&quot; , &quot;'&quot; , &quot;t forget to send us the proof of payment. Thanks!🙇‍♂️🙏&quot;) or . = concat(&quot;🔥💲*Payment Reminder Notification*

Hello! 

Thank you for your purchase 😀

=========

 you have an outstanding order for:

💰*Total: HKD 10*

=========

We&quot; , &quot;'&quot; , &quot;d like to remind you that you can make your payment through scanning the QR Code or the link/s below.

 🙆‍♂️🙇‍♀️Please don&quot; , &quot;'&quot; , &quot;t hesitate to reach out to us if you are facing any issues with the payments and don&quot; , &quot;'&quot; , &quot;t forget to send us the proof of payment. Thanks!🙇‍♂️🙏&quot;))]</value>
      <webElementGuid>1c4a37b7-9050-4c18-a687-cc35d2e32e68</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
