<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>b_Single Product 2</name>
   <tag></tag>
   <elementGuidId>a1136162-aebb-4da4-a171-bc9eb62f91e7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='3EB0CDFED20E740CAB2|1-content']/div[2]/div/b</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.flex-column > b</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>b</value>
      <webElementGuid>6d436878-a25f-40cc-9d2c-0485b27ab35b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Single Product 2</value>
      <webElementGuid>4e78b983-015b-4720-92af-7fdd38a37ce1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;3EB0CDFED20E740CAB2|1-content&quot;)/div[@class=&quot;product-container flex-between-center&quot;]/div[@class=&quot;flex-column&quot;]/b[1]</value>
      <webElementGuid>c97b05de-1ef7-4810-8236-06c331b753be</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='3EB0CDFED20E740CAB2|1-content']/div[2]/div/b</value>
      <webElementGuid>26bda74f-8651-48ca-ace5-c5c6d4bdc777</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='done_all'])[22]/following::b[1]</value>
      <webElementGuid>bbb51984-6642-4197-944f-b98963a0fa6c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='May 9, 2024 at 1:04 PM'])[1]/following::b[1]</value>
      <webElementGuid>f172a4e5-a8f7-4a9f-a65f-bd3e0974211c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='pending-ff5bd0b0211400d5'])[1]/preceding::b[1]</value>
      <webElementGuid>f377501a-ffad-4049-83e1-34b0a641e6ff</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='₱50.00'])[1]/preceding::b[1]</value>
      <webElementGuid>93a32a8b-558b-4006-b293-c31eb25e0092</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Single Product 2']/parent::*</value>
      <webElementGuid>233a1b35-6e93-4d55-8432-af485da1a5c6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/b</value>
      <webElementGuid>2febcfdd-1773-4a54-a9cf-d593d6e7d188</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//b[(text() = 'Single Product 2' or . = 'Single Product 2')]</value>
      <webElementGuid>19557c01-e067-4c9d-a0ea-d2dfcf455b5c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
