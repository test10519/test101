<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Logout (15)</name>
   <tag></tag>
   <elementGuidId>e46be4b3-683f-40c4-8ecf-0195324e8c69</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.MuiTypography-root.MuiTypography-smallRegular.css-1d2s0c2</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='logout'])[1]/following::span[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>e617096d-93c8-4d6f-afe2-35c1a6f612cf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiTypography-root MuiTypography-smallRegular css-1d2s0c2</value>
      <webElementGuid>8a90f467-c4ef-45e1-bd17-cd55d5e1cfd0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Logout</value>
      <webElementGuid>ae3c75b9-44b2-4c2b-978b-117a3519b54e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;hydrated&quot;]/body[1]/div[@class=&quot;MuiPopover-root MuiModal-root css-jp7szo&quot;]/div[@class=&quot;MuiPaper-root MuiPaper-elevation MuiPaper-rounded MuiPaper-elevation8 MuiPopover-paper css-6u8kux&quot;]/div[@class=&quot;MuiBox-root css-1r99qxv&quot;]/li[@class=&quot;MuiButtonBase-root MuiMenuItem-root MuiMenuItem-gutters MuiMenuItem-root MuiMenuItem-gutters css-105266g&quot;]/span[@class=&quot;MuiTypography-root MuiTypography-smallRegular css-1d2s0c2&quot;]</value>
      <webElementGuid>aa329df8-5e29-4633-9542-1d92060795ee</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='logout'])[1]/following::span[1]</value>
      <webElementGuid>03672a1a-e2f7-45bf-8c91-38de84c91d92</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Collect Locale URLS'])[1]/following::span[3]</value>
      <webElementGuid>53872298-15bc-4d0a-a0ad-d877d24ff880</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Logout']/parent::*</value>
      <webElementGuid>2102bbed-94f0-4138-8db1-a88d596e1b44</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[7]/span[2]</value>
      <webElementGuid>c74201ab-4c7b-4b49-b847-916bf687470e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Logout' or . = 'Logout')]</value>
      <webElementGuid>7139cf63-52c9-42fb-8988-4e8390c5a7da</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
