<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_MY Vacancy 4.0</name>
   <tag></tag>
   <elementGuidId>837b664b-90ef-4850-a67a-fbb1578652bb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div[3]/div[2]/div/div[2]/div[3]/div/div[2]/div/a/div[2]/div/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.MuiTypography-root.MuiTypography-baseSemiBold.css-whjneb</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>7fc7be63-a8e0-4cec-aa84-6e59f163eb0a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiTypography-root MuiTypography-baseSemiBold css-whjneb</value>
      <webElementGuid>f3f91cb0-e23c-4211-9653-3cf3193ecf01</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>MY Vacancy 4.0 🇲🇾✨</value>
      <webElementGuid>2a279040-8f98-4cb1-9dbd-7b6be0c0c8ad</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>MY Vacancy 4.0 🇲🇾✨</value>
      <webElementGuid>54336108-76b6-4bda-982b-4c64d3f6fc06</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column MuiGrid-wrap-xs-nowrap css-5u791s&quot;]/div[@class=&quot;MuiGrid-root css-qrdc5g&quot;]/div[@class=&quot;MuiGrid-root css-1r3ro9p&quot;]/div[@class=&quot;MuiGrid-root css-148h8t7&quot;]/div[@class=&quot;MuiGrid-root css-12u48ha&quot;]/div[1]/div[2]/div[1]/a[@class=&quot;MuiGrid-root css-46zlr0&quot;]/div[@class=&quot;MuiGrid-root css-1vlsgix&quot;]/div[@class=&quot;MuiGrid-root css-1mr0hd4&quot;]/span[@class=&quot;MuiTypography-root MuiTypography-baseSemiBold css-whjneb&quot;]</value>
      <webElementGuid>a2ea847c-46ba-464d-a798-8890bffc99be</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div[3]/div[2]/div/div[2]/div[3]/div/div[2]/div/a/div[2]/div/span</value>
      <webElementGuid>24afbd7b-3c85-4341-b769-6e0ffb90290f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='account_circle'])[2]/following::span[3]</value>
      <webElementGuid>8f9d0680-0b26-4fc3-aaba-d64626d96105</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bulk Actions'])[1]/following::span[4]</value>
      <webElementGuid>b7c5b992-62a6-40b6-addd-1a3e78af629b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='+ Add Tags'])[1]/preceding::span[3]</value>
      <webElementGuid>63f0c834-8924-49df-8476-cbd7b8635915</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='MY Vacancy 4.0 🇲🇾✨']/parent::*</value>
      <webElementGuid>4bb38664-b22a-4eca-957f-7f542897ca1e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/span</value>
      <webElementGuid>0a38886d-9ae0-41a4-87be-744150ceb96d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[@title = 'MY Vacancy 4.0 🇲🇾✨' and (text() = 'MY Vacancy 4.0 🇲🇾✨' or . = 'MY Vacancy 4.0 🇲🇾✨')]</value>
      <webElementGuid>40d66ffc-abd7-42c4-84c2-5a5ccc9967e7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
