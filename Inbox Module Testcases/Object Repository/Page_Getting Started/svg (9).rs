<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>svg (9)</name>
   <tag></tag>
   <elementGuidId>b2efa06c-c6cd-4dde-9fac-7d3644bf46a4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='fullscreen_exit'])[1]/preceding::*[name()='svg'][7]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.MuiGrid-root.css-1d2hfx > div.MuiGrid-root.css-wivtdb > div > svg</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>svg</value>
      <webElementGuid>32682632-a5ba-4139-9f77-ece6bde029ea</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xmlns</name>
      <type>Main</type>
      <value>http://www.w3.org/2000/svg</value>
      <webElementGuid>78d22ad1-e6aa-404f-939b-e6294f496896</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xmlns:xlink</name>
      <type>Main</type>
      <value>http://www.w3.org/1999/xlink</value>
      <webElementGuid>5447ee2d-7e0a-4e6d-99d5-9e6002d4eb9f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>viewBox</name>
      <type>Main</type>
      <value>0 0 500 500</value>
      <webElementGuid>bcc0bfb3-6cb3-4ed2-aac8-1731b244f08c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>width</name>
      <type>Main</type>
      <value>500</value>
      <webElementGuid>b0949b1f-4752-4dcb-9ce2-48d3de41186a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>height</name>
      <type>Main</type>
      <value>500</value>
      <webElementGuid>50e832bc-65b2-4534-a077-d2c97b745b70</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>preserveAspectRatio</name>
      <type>Main</type>
      <value>xMidYMid meet</value>
      <webElementGuid>b03247f8-a4b4-49d4-85bc-7bac24fb1d64</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;MuiCollapse-root MuiCollapse-horizontal MuiCollapse-entered css-yhefzq&quot;]/div[@class=&quot;MuiCollapse-wrapper MuiCollapse-horizontal css-164swfl&quot;]/div[@class=&quot;MuiCollapse-wrapperInner MuiCollapse-horizontal css-1mziwkx&quot;]/div[@class=&quot;MuiBox-root css-gxs633&quot;]/div[@class=&quot;MuiGrid-root css-1d2hfx&quot;]/div[@class=&quot;MuiGrid-root css-wivtdb&quot;]/div[1]/svg[1]</value>
      <webElementGuid>c3da939f-1430-4808-ab58-d03257a82cc1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='fullscreen_exit'])[1]/preceding::*[name()='svg'][7]</value>
      <webElementGuid>3227be17-6fd3-4f33-ba55-68ed523a61e2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
