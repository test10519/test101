<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_account_circle</name>
   <tag></tag>
   <elementGuidId>c11476fb-43fd-4106-a0c2-eb0406351b02</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div[3]/div[2]/div/div/div/div[2]/button[2]/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.material-symbols-outlined.MuiBox-root.css-11ahsy5</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>2006693b-38d4-44fe-95aa-77ac68f0a717</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>material-symbols-outlined MuiBox-root css-11ahsy5</value>
      <webElementGuid>9088b620-bc2e-40e3-8335-d33e2f571937</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>account_circle</value>
      <webElementGuid>ac5b76c7-89fe-4b4d-8b18-87ae1f1bb248</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column MuiGrid-wrap-xs-nowrap css-5u791s&quot;]/div[@class=&quot;MuiCollapse-root MuiCollapse-vertical MuiCollapse-entered css-c4sutr&quot;]/div[@class=&quot;MuiCollapse-wrapper MuiCollapse-vertical css-hboir5&quot;]/div[@class=&quot;MuiCollapse-wrapperInner MuiCollapse-vertical css-8atqhb&quot;]/div[@class=&quot;MuiGrid-root css-42de2x&quot;]/div[@class=&quot;MuiGrid-root css-1ymz3lr&quot;]/button[@class=&quot;MuiButtonBase-root MuiIconButton-root MuiIconButton-sizeMedium css-1jwxc8t&quot;]/span[@class=&quot;material-symbols-outlined MuiBox-root css-11ahsy5&quot;]</value>
      <webElementGuid>a22073bb-fc9b-4513-9d33-13bd319b7ddb</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div[3]/div[2]/div/div/div/div[2]/button[2]/span</value>
      <webElementGuid>f7a252d6-63fc-4cf4-8ec7-b75eb6ad2560</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='help'])[1]/following::span[2]</value>
      <webElementGuid>4136952e-8fce-4445-81a3-4c2d97ead5c3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='fullscreen'])[1]/following::span[4]</value>
      <webElementGuid>e7b338a9-8a5e-405b-a3f7-c5dc7e6be765</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Hello, Jo!'])[1]/preceding::span[4]</value>
      <webElementGuid>5bfaf4d0-bcdd-4eee-aac8-f933fe0e1859</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='lan'])[1]/preceding::span[5]</value>
      <webElementGuid>207634a8-7b24-491c-93be-75f24b8f8182</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='account_circle']/parent::*</value>
      <webElementGuid>cca5fbd5-66b2-4b80-b53e-82b888b3fbf6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button[2]/span</value>
      <webElementGuid>af67c7a0-147a-47c6-80ce-100f28b0d50d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'account_circle' or . = 'account_circle')]</value>
      <webElementGuid>4ca7d26f-0695-4520-97a4-1c88515e8fa5</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
