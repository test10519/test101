<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>svg (41) (2) (2) (2)</name>
   <tag></tag>
   <elementGuidId>07ad5248-999c-4711-9248-ca76efeb11fe</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='fullscreen_exit'])[1]/preceding::*[name()='svg'][8]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.MuiBox-root.css-0 > div.MuiGrid-root.css-wivtdb > div > svg</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>svg</value>
      <webElementGuid>2cec744c-c320-4f14-9206-6bfa96f1710b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xmlns</name>
      <type>Main</type>
      <value>http://www.w3.org/2000/svg</value>
      <webElementGuid>0220e5f8-f301-41d2-b7b2-f82c8123c713</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xmlns:xlink</name>
      <type>Main</type>
      <value>http://www.w3.org/1999/xlink</value>
      <webElementGuid>3c36458a-a442-4c59-94ae-738ee28548f0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>viewBox</name>
      <type>Main</type>
      <value>0 0 500 500</value>
      <webElementGuid>0b105f9e-ecf5-4c6b-905a-4195dce4ab57</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>width</name>
      <type>Main</type>
      <value>500</value>
      <webElementGuid>0aae34a7-d364-4c36-a9bf-50f4950777b1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>height</name>
      <type>Main</type>
      <value>500</value>
      <webElementGuid>8ca92312-e5c7-481d-9ad9-29b8071f55ee</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>preserveAspectRatio</name>
      <type>Main</type>
      <value>xMidYMid meet</value>
      <webElementGuid>7dad2040-0155-4a91-949a-9cd1731f1cae</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;MuiCollapse-root MuiCollapse-horizontal MuiCollapse-entered css-yhefzq&quot;]/div[@class=&quot;MuiCollapse-wrapper MuiCollapse-horizontal css-164swfl&quot;]/div[@class=&quot;MuiCollapse-wrapperInner MuiCollapse-horizontal css-1mziwkx&quot;]/div[@class=&quot;MuiBox-root css-gxs633&quot;]/div[@class=&quot;MuiGrid-root css-1d2hfx&quot;]/div[@class=&quot;MuiBox-root css-0&quot;]/div[@class=&quot;MuiGrid-root css-wivtdb&quot;]/div[1]/svg[1]</value>
      <webElementGuid>3e85e6a6-affb-466b-93bb-443c54b8aa96</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='fullscreen_exit'])[1]/preceding::*[name()='svg'][8]</value>
      <webElementGuid>3a40993a-ad44-4e38-982a-d92a2403f627</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
