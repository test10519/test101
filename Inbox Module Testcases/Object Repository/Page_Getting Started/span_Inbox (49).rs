<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Inbox (49)</name>
   <tag></tag>
   <elementGuidId>758b7df1-a08f-45f1-acba-e4e8b9de3e1c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.MuiTypography-root.MuiTypography-baseMedium.css-57mpad</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Inbox'])[1]/following::span[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>5375dfe0-9488-4b25-92fb-8c516d344e6e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiTypography-root MuiTypography-baseMedium css-57mpad</value>
      <webElementGuid>341e6242-42bd-42e1-8604-dfb812234773</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Inbox</value>
      <webElementGuid>b4307e5d-091e-494a-9609-2c5f12e268fc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;hydrated&quot;]/body[1]/div[@class=&quot;base-Popper-root css-b82va4&quot;]/a[@class=&quot;MuiGrid-root css-1vo1ped&quot;]/span[@class=&quot;MuiTypography-root MuiTypography-baseMedium css-57mpad&quot;]</value>
      <webElementGuid>616af147-dccc-4ec4-aaac-cde622d91cd0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Inbox'])[1]/following::span[1]</value>
      <webElementGuid>737bb9ae-84b3-40d9-86ef-b687b2d78b41</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='arrow_forward_ios'])[8]/following::span[3]</value>
      <webElementGuid>d8fd251f-ae1b-450b-b481-4e1d1df03a06</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Invite Teammate'])[1]/preceding::span[1]</value>
      <webElementGuid>16c5a18b-f98c-44c8-89a2-1d6a0a66a41a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Channels'])[1]/preceding::span[2]</value>
      <webElementGuid>e80895f4-3723-48e7-8c1d-1d3979fc6097</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/a/span</value>
      <webElementGuid>ab4f7ca9-74c4-4880-b1d0-3d2bddb60fef</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Inbox' or . = 'Inbox')]</value>
      <webElementGuid>3da10def-238c-4d81-8054-8b0a14fdfbb1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
