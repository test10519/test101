<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Enter NameCreate</name>
   <tag></tag>
   <elementGuidId>2f358964-d339-4a92-a613-c79bac7dfa5b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Partner'])[1]/following::div[15]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.MuiPaper-root.MuiPaper-25.MuiPaper-rounded.MuiPopover-paper.css-ih3y72</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>6ae4c5de-5412-4e5a-b14d-09e30f6be188</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiPaper-root MuiPaper-25 MuiPaper-rounded MuiPopover-paper css-ih3y72</value>
      <webElementGuid>7022727f-30af-469c-874e-38ae6fd83594</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>-1</value>
      <webElementGuid>edf894b4-fd2b-48c3-ab2c-491634aab8e0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Enter Name​Create</value>
      <webElementGuid>960139ba-bb9f-443f-b333-430b484684f0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;hydrated&quot;]/body[1]/div[@class=&quot;MuiPopover-root MuiModal-root css-jp7szo&quot;]/div[@class=&quot;MuiPaper-root MuiPaper-25 MuiPaper-rounded MuiPopover-paper css-ih3y72&quot;]</value>
      <webElementGuid>2e511da6-b008-41ff-a080-ba65b27cd516</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Partner'])[1]/following::div[15]</value>
      <webElementGuid>a4a92aae-626b-4c18-95ef-fce2efcaaccb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Subscription End Date:'])[1]/following::div[16]</value>
      <webElementGuid>35b4f2da-bb74-48e5-9d2b-7833951a61d9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div[3]</value>
      <webElementGuid>aa2dc3e8-1e5f-4797-8230-e77a9a8d2e29</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Enter Name​Create' or . = 'Enter Name​Create')]</value>
      <webElementGuid>b2fac565-d56e-4cfc-91a3-57ccdcff48be</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
