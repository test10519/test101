<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Jo test (2)</name>
   <tag></tag>
   <elementGuidId>b74fd0e2-8426-4054-9a27-482b8ac3f075</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.MuiTypography-root.MuiTypography-mediumSemiBold.css-1t8dp5s</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='inbox-header']/div/div/div/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>c9b21dcc-c125-49a0-8773-88fa94a271b9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiTypography-root MuiTypography-mediumSemiBold css-1t8dp5s</value>
      <webElementGuid>6bac4ab4-72b7-4ea9-a6f6-36364867a25e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>contenteditable</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>8456b831-2ed6-4c6a-96da-edc331a98037</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Jo test</value>
      <webElementGuid>4158925c-5482-40c1-b90c-80eb416ee44f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;inbox-header&quot;)/div[@class=&quot;MuiGrid-root css-u2a8x6&quot;]/div[@class=&quot;MuiGrid-root tour-contact-info css-7bcfnq&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-wrap-xs-nowrap css-jznt39&quot;]/span[@class=&quot;MuiTypography-root MuiTypography-mediumSemiBold css-1t8dp5s&quot;]</value>
      <webElementGuid>c90fe7bc-7795-4673-b9c2-5f5a8c02eee4</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='inbox-header']/div/div/div/span</value>
      <webElementGuid>d6a9cf83-bc43-453f-9db3-30bb78e99842</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='account_circle'])[9]/following::span[1]</value>
      <webElementGuid>e25dca9b-8b98-4a9f-b8a0-ea5f414b03e3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='expand_more'])[10]/following::span[4]</value>
      <webElementGuid>22c42291-0fc8-4544-bf4f-4c63fa8836a3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='edit'])[1]/preceding::span[1]</value>
      <webElementGuid>a55fcd98-93ba-46c1-9768-b27d63172e02</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='phone'])[1]/preceding::span[3]</value>
      <webElementGuid>b940e7a0-972e-4b17-84f1-df0228a61b3f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div[3]/div/div/div/div/span</value>
      <webElementGuid>77417178-2323-4394-a842-204e77902a39</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Jo test' or . = 'Jo test')]</value>
      <webElementGuid>4c1cb877-4a73-4cc2-a2ce-eae2d3066538</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
