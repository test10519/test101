<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_add (1)</name>
   <tag></tag>
   <elementGuidId>2b9745c8-6d7e-49d7-9a2e-2c757c5582f4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.MuiButtonBase-root.MuiIconButton-root.MuiIconButton-colorPrimary.MuiIconButton-sizeMedium.css-1l9d2tp > span.material-symbols-outlined.MuiBox-root.css-0</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div[3]/div[3]/div/div[3]/div[2]/div[2]/div/div[2]/div/div/button/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>b2a1f27c-5ff3-4613-998b-baa793c201d1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>material-symbols-outlined MuiBox-root css-0</value>
      <webElementGuid>f8849c24-4e09-4983-a1eb-2a3b0acc05d0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>add</value>
      <webElementGuid>b497b834-eb96-4b9b-972f-0cd51b8e079f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column MuiGrid-wrap-xs-nowrap css-5u791s&quot;]/div[@class=&quot;MuiGrid-root css-qrdc5g&quot;]/div[@class=&quot;MuiGrid-root css-1r3ro9p&quot;]/div[@class=&quot;MuiBox-root css-13tphjr&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-wrap-xs-nowrap css-1112fw4&quot;]/div[@class=&quot;MuiGrid-root css-1fghw63&quot;]/div[@class=&quot;MuiGrid-root css-1eiz13t&quot;]/div[@class=&quot;MuiGrid-root css-7j22ch&quot;]/div[@class=&quot;MuiGrid-root css-1c8gsuk&quot;]/div[@class=&quot;MuiGrid-root css-yzfd7p&quot;]/button[@class=&quot;MuiButtonBase-root MuiIconButton-root MuiIconButton-colorPrimary MuiIconButton-sizeMedium css-1l9d2tp&quot;]/span[@class=&quot;material-symbols-outlined MuiBox-root css-0&quot;]</value>
      <webElementGuid>08aaec6e-094a-4467-882b-c22dea8e5ba1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div[3]/div[3]/div/div[3]/div[2]/div[2]/div/div[2]/div/div/button/span</value>
      <webElementGuid>ab4c59ea-45a0-40fa-9f5a-e81242048d46</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='variable_insert'])[1]/following::span[1]</value>
      <webElementGuid>30ee21c5-2955-44ed-8c9b-2412ed252b09</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='arrow_right'])[1]/following::span[2]</value>
      <webElementGuid>b7cba979-91e9-4f2d-94ef-26d1786234fc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='drag_indicator'])[1]/preceding::span[4]</value>
      <webElementGuid>2e7c5a97-55d7-4d8a-8ffe-ccc8eb8ed81f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='​'])[3]/preceding::span[5]</value>
      <webElementGuid>e512fa9a-151d-4cd1-9087-d90284571ac2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[2]/div/div/button/span</value>
      <webElementGuid>9a6a3ee7-9934-433e-9a07-81111f63cee7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'add' or . = 'add')]</value>
      <webElementGuid>8b1b3911-97ea-4e65-97d7-5901fc0e71e8</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
