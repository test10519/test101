<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_Ivan Bentley</name>
   <tag></tag>
   <elementGuidId>0f6393a8-54f1-4e9e-919d-ecef9e343927</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.MuiButtonBase-root.MuiButton-root.MuiButton-text.MuiButton-textPrimary.MuiButton-sizeMedium.MuiButton-textSizeMedium.MuiButton-colorPrimary.MuiButton-root.MuiButton-text.MuiButton-textPrimary.MuiButton-sizeMedium.MuiButton-textSizeMedium.MuiButton-colorPrimary.css-r7pc1q > p.MuiTypography-root.MuiTypography-body1.css-adhdfs</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='inbox-header']/div[2]/div/button/p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>f0b55d93-b7da-49f5-8543-3a48f2152f5c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiTypography-root MuiTypography-body1 css-adhdfs</value>
      <webElementGuid>7b898072-6fe8-4979-a22c-c6e6f9bb93e5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Ivan Bentley</value>
      <webElementGuid>53c68975-364f-4cc3-9979-0065d626d112</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;inbox-header&quot;)/div[@class=&quot;MuiGrid-root css-fqgfhr&quot;]/div[@class=&quot;MuiGrid-root css-yzfd7p&quot;]/button[@class=&quot;MuiButtonBase-root MuiButton-root MuiButton-text MuiButton-textPrimary MuiButton-sizeMedium MuiButton-textSizeMedium MuiButton-colorPrimary MuiButton-root MuiButton-text MuiButton-textPrimary MuiButton-sizeMedium MuiButton-textSizeMedium MuiButton-colorPrimary css-r7pc1q&quot;]/p[@class=&quot;MuiTypography-root MuiTypography-body1 css-adhdfs&quot;]</value>
      <webElementGuid>1b9adc27-0c31-4a78-abed-92cd4f578bca</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='inbox-header']/div[2]/div/button/p</value>
      <webElementGuid>2b9057bd-89f1-47d5-8602-652d6749125e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='call_received'])[1]/following::p[2]</value>
      <webElementGuid>8c90b981-71c8-4ce8-a3eb-62b098ba6659</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='call_made'])[1]/following::p[3]</value>
      <webElementGuid>5bc8efe8-5b85-43b2-908b-2ece7e3d0568</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='expand_more'])[11]/preceding::p[1]</value>
      <webElementGuid>08c39d64-81f5-4732-8c17-f01b263d34d2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='done'])[3]/preceding::p[1]</value>
      <webElementGuid>fd578a5d-d106-496e-9ee3-0f0fdf87f881</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[2]/div/button/p</value>
      <webElementGuid>3fdda593-3c94-495c-905a-4d8f59d541d0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'Ivan Bentley' or . = 'Ivan Bentley')]</value>
      <webElementGuid>8abe8b8d-aef7-45b2-9359-25d07167b845</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
