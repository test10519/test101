<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_expand_more</name>
   <tag></tag>
   <elementGuidId>679ae141-42aa-4492-b412-1eb97fdf4202</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.material-symbols-rounded.MuiBox-root.css-1d42fir</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='inbox-header']/div[2]/div/button/span/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>f0f03095-c253-477e-9a66-dc786bff7894</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>material-symbols-rounded MuiBox-root css-1d42fir</value>
      <webElementGuid>5aca3fde-fac6-484f-9a17-a9ec7902240c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>expand_more</value>
      <webElementGuid>a982059d-19bc-4bdf-b90b-31c77e9198b9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;inbox-header&quot;)/div[@class=&quot;MuiGrid-root css-fqgfhr&quot;]/div[@class=&quot;MuiGrid-root css-yzfd7p&quot;]/button[@class=&quot;MuiButtonBase-root MuiButton-root MuiButton-text MuiButton-textSecondary MuiButton-sizeMedium MuiButton-textSizeMedium MuiButton-colorSecondary MuiButton-root MuiButton-text MuiButton-textSecondary MuiButton-sizeMedium MuiButton-textSizeMedium MuiButton-colorSecondary css-3vjmwg&quot;]/span[@class=&quot;MuiButton-icon MuiButton-endIcon MuiButton-iconSizeMedium css-pt151d&quot;]/span[@class=&quot;material-symbols-rounded MuiBox-root css-1d42fir&quot;]</value>
      <webElementGuid>e09d8bb1-92ac-4c4d-a391-0ce2c8475397</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='inbox-header']/div[2]/div/button/span/span</value>
      <webElementGuid>8be979fa-7abf-40fe-a751-f65ca88ecb62</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='call_received'])[1]/following::span[2]</value>
      <webElementGuid>17061499-a4db-4b39-84b0-fe1fbb2a9c97</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='done'])[3]/preceding::span[5]</value>
      <webElementGuid>daa266a3-8cd9-46fc-a3bf-e213581b6c40</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='search'])[2]/preceding::span[7]</value>
      <webElementGuid>0ab4f453-fcca-4fd5-ae34-532102462bba</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[2]/div/button/span/span</value>
      <webElementGuid>1037f6b1-adff-4bcb-9b15-73e20f1dbdc0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'expand_more' or . = 'expand_more')]</value>
      <webElementGuid>b5ed0165-b92d-4167-8608-c6a2e64ce259</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
