<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_search_r1d</name>
   <tag></tag>
   <elementGuidId>d1e3ffb0-98a7-4e85-8551-0cd8220954df</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id=':r1d:']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>c5693a50-6ef3-4a08-b8b2-20ef9b79114b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-invalid</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>f2b6faaf-9d3a-4603-9f13-a2271efaac35</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>:r1d:</value>
      <webElementGuid>58c38382-2524-40fa-839f-528261a44d5f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Search...</value>
      <webElementGuid>7e596bc2-6cd4-4581-a767-1c9d826cc5d7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>19d180ed-a710-4eec-b340-7491e393d3ce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiInputBase-input MuiOutlinedInput-input MuiInputBase-inputAdornedStart MuiInputBase-inputAdornedEnd css-1tbj1lq</value>
      <webElementGuid>e9dcf718-1e25-4db3-9123-717fab8d1232</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>T</value>
      <webElementGuid>1f063ae6-c7d1-47c6-8abb-a3fb792cbae4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;:r1d:&quot;)</value>
      <webElementGuid>8496991c-06f4-4464-8129-4610b05ddcfb</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id=':r1d:']</value>
      <webElementGuid>23eee060-fd0e-4952-94af-85ce7284595c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/input</value>
      <webElementGuid>84c00336-309b-4dcb-a5d5-31886ee42251</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = ':r1d:' and @placeholder = 'Search...' and @type = 'text']</value>
      <webElementGuid>7b739e3d-1a92-4702-867e-93faffa2768d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
