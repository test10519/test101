<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_63 926 150 5795</name>
   <tag></tag>
   <elementGuidId>44304a99-33aa-4cea-9772-de092f648d9d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='inbox-header']/div[2]/div/div/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.MuiTypography-root.MuiTypography-xSmallRegular.css-17psejy</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>bd895ba9-463f-4a09-ae9e-fbcd3e04e7bc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiTypography-root MuiTypography-xSmallRegular css-17psejy</value>
      <webElementGuid>5693229e-ca5e-4989-9ed8-5d5f88a3f0a6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>+63 926 150 5795</value>
      <webElementGuid>209acf2e-8c59-4318-a9b4-f417084abbf4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>+63 926 150 5795</value>
      <webElementGuid>fa1180f3-ec4f-4717-aef0-47e35d5c71a0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;inbox-header&quot;)/div[@class=&quot;MuiGrid-root css-fqgfhr&quot;]/div[@class=&quot;MuiGrid-root css-yzfd7p&quot;]/div[@class=&quot;MuiGrid-root css-c1vp97&quot;]/span[@class=&quot;MuiTypography-root MuiTypography-xSmallRegular css-17psejy&quot;]</value>
      <webElementGuid>f2007767-a62a-47cc-bd4a-0a0391074982</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='inbox-header']/div[2]/div/div/span</value>
      <webElementGuid>596a23da-71e5-424e-af67-b639b108b781</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='expand_more'])[11]/following::span[2]</value>
      <webElementGuid>71447412-0d43-4889-8214-b185f2807efb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='call_received'])[1]/following::span[4]</value>
      <webElementGuid>f6d7bddf-5171-4b7f-bb27-a0faa900ed22</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='search'])[2]/preceding::span[1]</value>
      <webElementGuid>3b049053-7213-4946-a6c4-fa0f11c59589</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='expand_more'])[12]/preceding::span[3]</value>
      <webElementGuid>fe87798b-0818-4282-ba0a-caf7782a8218</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div[2]/div/div/span</value>
      <webElementGuid>8a6540f6-e56b-4630-82e5-971055cd0cda</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[@title = '+63 926 150 5795' and (text() = '+63 926 150 5795' or . = '+63 926 150 5795')]</value>
      <webElementGuid>1680c29c-eae1-4ad8-8cfc-0fff843a97ac</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
