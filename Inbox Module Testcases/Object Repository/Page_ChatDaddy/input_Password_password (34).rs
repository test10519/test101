<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Password_password (34)</name>
   <tag></tag>
   <elementGuidId>0c3042ac-7d21-43c5-bb52-30cd8e128944</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id=':r2:']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>8413de69-3b78-44d8-918b-9adc44027dec</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-invalid</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>3f67ed48-1818-4eac-a122-30bc9eec180f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>:r2:</value>
      <webElementGuid>bf6f395d-b948-4148-aa74-da5014deb545</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>password</value>
      <webElementGuid>88b3a712-0d49-4f70-90c9-988149f44af5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>••••••••••••</value>
      <webElementGuid>698dc9e5-6be9-4542-a349-6ef18fe6c3c1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>password</value>
      <webElementGuid>8e87f376-d7d6-4358-b31b-5a3a795f92bd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiInputBase-input MuiOutlinedInput-input css-1rpfghe</value>
      <webElementGuid>ebcfe6c8-6cf1-4345-9070-633208870d05</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>@</value>
      <webElementGuid>14a129d3-4935-4301-9f0e-5828a408845d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;:r2:&quot;)</value>
      <webElementGuid>53595c67-45db-410e-af28-9991d1938a58</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id=':r2:']</value>
      <webElementGuid>07519ddc-e7b7-4b5d-b1b3-e190d21af3f1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div[2]/div/div/div[2]/div[2]/div/div/input</value>
      <webElementGuid>14e63ca1-c885-4afb-bcbe-440ad25b4273</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/input</value>
      <webElementGuid>8d28557a-f5aa-4259-a786-8430b1f09a06</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = ':r2:' and @name = 'password' and @placeholder = '••••••••••••' and @type = 'password']</value>
      <webElementGuid>001a1016-8e9b-4e58-aa18-4a6e191036e9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
