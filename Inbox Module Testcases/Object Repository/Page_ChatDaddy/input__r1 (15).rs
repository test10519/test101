<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input__r1 (15)</name>
   <tag></tag>
   <elementGuidId>051bed32-d8dc-483d-bcf2-6daac52ffe79</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id=':r1:']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>b2661ca2-bd1f-404d-b3bd-0e3790d68586</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-invalid</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>4a50213f-ce4a-451b-93eb-3d5f0384d0f8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>:r1:</value>
      <webElementGuid>f98a5b15-c6c5-4a1a-a665-7d9bb3c14559</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>tel</value>
      <webElementGuid>82f82a9b-e4d8-421f-b66e-2e115bc2b4c5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiInputBase-input MuiOutlinedInput-input MuiInputBase-inputAdornedStart css-w2xudr</value>
      <webElementGuid>650295ed-50eb-4751-bcfd-3496d1e41d3d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>+63 9</value>
      <webElementGuid>f0ccf17b-db4c-46c6-83d7-78d88646f2c6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;:r1:&quot;)</value>
      <webElementGuid>e49d0472-5d96-4efa-a441-917024738034</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id=':r1:']</value>
      <webElementGuid>bd37efbe-713b-45f0-b1ca-66f191518753</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div[2]/div/div/div[2]/div/div/div/input</value>
      <webElementGuid>8aba9f4d-4d1e-4a98-b47d-f33312db5c33</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//input</value>
      <webElementGuid>3d04fe62-cedc-481d-a7ee-5daf99e64710</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = ':r1:' and @type = 'tel']</value>
      <webElementGuid>c5d32887-35fd-4d87-a943-5ab9a070c68e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
