<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Skip this Short and Sweet Tour (3)</name>
   <tag></tag>
   <elementGuidId>82c440d5-721a-4156-b0fa-29f03cc27e35</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.MuiTypography-root.MuiTypography-smallSemiBold.css-1ocob9</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='react-joyride-step-0']/div/div/div/div[2]/button[2]/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>79ff4cb1-e0c2-42f5-a260-c108e692c531</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiTypography-root MuiTypography-smallSemiBold css-1ocob9</value>
      <webElementGuid>7ac5c269-0928-4bf9-9a8b-2ee373d448af</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Skip this Short and Sweet Tour</value>
      <webElementGuid>d3588ac0-64e1-4e77-8a64-434a6d5e0cee</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;react-joyride-step-0&quot;)/div[@class=&quot;__floater __floater__open&quot;]/div[@class=&quot;__floater__body&quot;]/div[@class=&quot;MuiGrid-root css-grxqdy&quot;]/div[@class=&quot;MuiGrid-root css-b486k7&quot;]/button[@class=&quot;MuiButtonBase-root MuiButton-root MuiButton-text MuiButton-textPrimary MuiButton-sizeMedium MuiButton-textSizeMedium MuiButton-colorPrimary MuiButton-root MuiButton-text MuiButton-textPrimary MuiButton-sizeMedium MuiButton-textSizeMedium MuiButton-colorPrimary css-iotrky&quot;]/span[@class=&quot;MuiTypography-root MuiTypography-smallSemiBold css-1ocob9&quot;]</value>
      <webElementGuid>d8518f71-9b27-42af-b7a9-a8366adcb774</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='react-joyride-step-0']/div/div/div/div[2]/button[2]/span</value>
      <webElementGuid>ccd3df5e-55e8-4396-8bf1-b916d64826e5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Take the Tour'])[1]/following::span[2]</value>
      <webElementGuid>819bd2da-6537-45ab-aef6-482eaf2069d5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='It’s going to take just a few seconds.'])[1]/following::span[3]</value>
      <webElementGuid>9f7c28c1-07b7-483c-9c02-c5a8b9617a41</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Skip this Short and Sweet Tour']/parent::*</value>
      <webElementGuid>174b990c-a585-40ba-8781-4b92f0808961</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/div/div/div[2]/button[2]/span</value>
      <webElementGuid>a5c24b1c-9d0b-4dad-b4d3-0f64d1d526f5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Skip this Short and Sweet Tour' or . = 'Skip this Short and Sweet Tour')]</value>
      <webElementGuid>17923cc3-a16e-4105-993e-8ff79bb17e93</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
