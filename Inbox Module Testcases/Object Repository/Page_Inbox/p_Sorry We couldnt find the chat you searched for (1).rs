<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_Sorry We couldnt find the chat you searched for (1)</name>
   <tag></tag>
   <elementGuidId>b1c032ba-1ccd-4175-8660-4b0677c8fb40</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div[3]/div[3]/div/div[2]/div[3]/p</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>p.MuiTypography-root.MuiTypography-body2.css-9pm7v1</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>dd9dc203-fdb3-409e-9715-25afb8bdfc30</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiTypography-root MuiTypography-body2 css-9pm7v1</value>
      <webElementGuid>48361a1a-4600-4999-b6f8-f2423d7c4a1d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Sorry! We couldn’t find the chat you searched for.</value>
      <webElementGuid>5e106be2-2bc0-456c-95ac-d15893df42c8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column MuiGrid-wrap-xs-nowrap css-5u791s&quot;]/div[@class=&quot;MuiGrid-root css-qrdc5g&quot;]/div[@class=&quot;MuiGrid-root css-1r3ro9p&quot;]/div[@class=&quot;MuiGrid-root css-148h8t7&quot;]/div[@class=&quot;MuiGrid-root css-12u48ha&quot;]/p[@class=&quot;MuiTypography-root MuiTypography-body2 css-9pm7v1&quot;]</value>
      <webElementGuid>630b17dc-d7e3-4257-85d8-64eae2cc70a3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div[3]/div[3]/div/div[2]/div[3]/p</value>
      <webElementGuid>215eee65-9ba2-4f45-bd4d-9f5d743de1f0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bulk Actions'])[1]/following::p[1]</value>
      <webElementGuid>a307821f-6633-44cc-9842-491ef8c0856e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='arrow_drop_down'])[4]/following::p[2]</value>
      <webElementGuid>4609cb6c-4f08-4cc7-8a18-d5d04ad0f23c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Search Messages Instead?'])[1]/preceding::p[1]</value>
      <webElementGuid>cab7c28f-a66d-487c-b163-c862442a37f2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Search Messages Instead?'])[2]/preceding::p[1]</value>
      <webElementGuid>7aabe6a8-93c0-45c7-a077-345ac415f745</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Sorry! We couldn’t find the chat you searched for.']/parent::*</value>
      <webElementGuid>58b1b5e3-eecc-4ced-a328-38091373c13d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/p</value>
      <webElementGuid>c9a97748-57d8-4a41-94fb-b8a63c4c60ae</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'Sorry! We couldn’t find the chat you searched for.' or . = 'Sorry! We couldn’t find the chat you searched for.')]</value>
      <webElementGuid>c4482817-4862-4fb3-ae02-96801926d46a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
