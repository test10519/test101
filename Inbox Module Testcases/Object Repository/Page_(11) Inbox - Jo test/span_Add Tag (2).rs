<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Add Tag (2)</name>
   <tag></tag>
   <elementGuidId>6e7ab5f6-f1e6-4fc0-82d5-e450d0b849e7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div[3]/div[3]/div/div[3]/div[2]/div[2]/div/div/button/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.MuiTypography-root.MuiTypography-smallMedium.css-13dgrh4</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>5150fac4-32ed-493f-b2db-bb56c49f8150</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiTypography-root MuiTypography-smallMedium css-13dgrh4</value>
      <webElementGuid>56fe9da4-0d75-47c8-b4a1-4b50b5ddbb5e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>+ Add Tag</value>
      <webElementGuid>3ed84838-7537-40ad-a421-b66c943373fb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column MuiGrid-wrap-xs-nowrap css-5u791s&quot;]/div[@class=&quot;MuiGrid-root css-qrdc5g&quot;]/div[@class=&quot;MuiGrid-root css-1r3ro9p&quot;]/div[@class=&quot;MuiBox-root css-13tphjr&quot;]/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-wrap-xs-nowrap css-1112fw4&quot;]/div[@class=&quot;MuiGrid-root css-1fghw63&quot;]/div[@class=&quot;MuiGrid-root css-1eiz13t&quot;]/div[@class=&quot;MuiGrid-root css-3q639z&quot;]/button[@class=&quot;MuiButtonBase-root MuiButton-root MuiButton-text MuiButton-textPrimary MuiButton-sizeMedium MuiButton-textSizeMedium MuiButton-colorPrimary MuiButton-root MuiButton-text MuiButton-textPrimary MuiButton-sizeMedium MuiButton-textSizeMedium MuiButton-colorPrimary css-9o3257&quot;]/span[@class=&quot;MuiTypography-root MuiTypography-smallMedium css-13dgrh4&quot;]</value>
      <webElementGuid>3d3bf3b5-abb8-485d-af9b-2eeed165fa98</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div[3]/div[3]/div/div[3]/div[2]/div[2]/div/div/button/span</value>
      <webElementGuid>084842ad-baed-48e5-bd51-994c9d9140f8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='close'])[1]/following::span[2]</value>
      <webElementGuid>8e9d2c9d-7756-47d3-9135-4e44b7fa6b1a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Drop Files Here'])[1]/following::span[3]</value>
      <webElementGuid>64e090ff-e98d-48a2-8fc0-59d61837efcd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='VIP'])[2]/preceding::span[1]</value>
      <webElementGuid>7a394c05-9759-42a0-aa99-539bcdaaa2be</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='New Tag'])[2]/preceding::span[2]</value>
      <webElementGuid>731559fe-2c4d-4c63-bd9d-058eb3abd981</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='+ Add Tag']/parent::*</value>
      <webElementGuid>7481025c-b066-45b1-87fb-992c8b42aa47</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div/div/button/span</value>
      <webElementGuid>85740fca-fbb4-44c0-a7cb-84bb5ee8291f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = '+ Add Tag' or . = '+ Add Tag')]</value>
      <webElementGuid>e3d5767c-148d-4af5-9e2b-1fe5431cc253</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
