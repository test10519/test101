<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_add</name>
   <tag></tag>
   <elementGuidId>e2b92cd8-e5b6-412a-b93b-41f088a462aa</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='​'])[16]/following::span[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.MuiButton-icon.MuiButton-endIcon.MuiButton-iconSizeMedium.css-pt151d > span.material-symbols-outlined.MuiBox-root.css-14d0z92</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>3c2edb65-e4a6-4bbe-84b9-70d9e62744e8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>material-symbols-outlined MuiBox-root css-14d0z92</value>
      <webElementGuid>597379e0-c9d3-41eb-9553-0839c965a191</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>add</value>
      <webElementGuid>e3326b4d-f47c-465c-b76a-df0f3c349e77</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;hydrated&quot;]/body[1]/div[@class=&quot;MuiPopover-root MuiModal-root css-jp7szo&quot;]/div[@class=&quot;MuiPaper-root MuiPaper-25 MuiPaper-rounded MuiPopover-paper css-1qk7w6v&quot;]/div[2]/div[2]/div[1]/button[@class=&quot;MuiButtonBase-root MuiButton-root MuiButton-text MuiButton-textPrimary MuiButton-sizeMedium MuiButton-textSizeMedium MuiButton-colorPrimary MuiButton-root MuiButton-text MuiButton-textPrimary MuiButton-sizeMedium MuiButton-textSizeMedium MuiButton-colorPrimary css-14vzcio&quot;]/span[@class=&quot;MuiButton-icon MuiButton-endIcon MuiButton-iconSizeMedium css-pt151d&quot;]/span[@class=&quot;material-symbols-outlined MuiBox-root css-14d0z92&quot;]</value>
      <webElementGuid>0e341cfb-e1f3-42db-97e6-d95d13311691</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='​'])[16]/following::span[2]</value>
      <webElementGuid>af00e139-4a0a-406b-8c55-e69e440bee0a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div/button/span/span</value>
      <webElementGuid>fe5c218b-fcdb-4b43-9f72-32de907c36f2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'add' or . = 'add')]</value>
      <webElementGuid>c03fe5d4-cb80-4ede-aaf8-cb731361a49f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
