<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_expand_more</name>
   <tag></tag>
   <elementGuidId>d71da4e1-aeba-4e25-8212-95924b27c573</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='inbox-header']/div[2]/button[3]/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>74aff556-a4a2-4d1c-8287-a8cf7db48600</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>material-symbols-outlined MuiBox-root css-1dvl6i1</value>
      <webElementGuid>996cb584-6284-406f-b119-6937951b1fc4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>expand_more</value>
      <webElementGuid>270e5920-fcd1-407e-a5b9-450ed805d833</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;inbox-header&quot;)/div[@class=&quot;MuiGrid-root css-fqgfhr&quot;]/button[@class=&quot;MuiButtonBase-root MuiIconButton-root MuiIconButton-sizeMedium css-vet8nc&quot;]/span[@class=&quot;material-symbols-outlined MuiBox-root css-1dvl6i1&quot;]</value>
      <webElementGuid>1848054c-e416-49e8-b4ad-81f31aac7e48</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='inbox-header']/div[2]/button[3]/span</value>
      <webElementGuid>52a4ab05-e4f5-48da-9f1d-5fa712986960</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='search'])[2]/following::span[2]</value>
      <webElementGuid>34da13c1-d424-42fc-8e4f-0dd36f87d58c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='done'])[2]/following::span[4]</value>
      <webElementGuid>e7913fdb-b394-4cc1-ab36-3d822aa129c1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jo Arao'])[1]/preceding::span[4]</value>
      <webElementGuid>7d89750d-c76c-448d-9aaa-a489654b551c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/button[3]/span</value>
      <webElementGuid>963e17a4-468d-4a63-ab71-d392970a7bb6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'expand_more' or . = 'expand_more')]</value>
      <webElementGuid>74fff577-dd58-422c-ae61-75f3335bd071</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
