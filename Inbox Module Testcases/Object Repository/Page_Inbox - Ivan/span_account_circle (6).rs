<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_account_circle (6)</name>
   <tag></tag>
   <elementGuidId>9c1eafad-833d-4758-8c5d-ecd7ef535150</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div[3]/div[2]/div/div/div/div[2]/button[2]/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.material-symbols-outlined.MuiBox-root.css-11ahsy5</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>85a98690-bd73-4c91-8c65-b077aa830211</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>material-symbols-outlined MuiBox-root css-11ahsy5</value>
      <webElementGuid>ac3d4dbb-be5f-4611-a654-10d522d7f97d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>account_circle</value>
      <webElementGuid>5dfd273a-014e-4d7b-a1ee-b0fc87638175</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column MuiGrid-wrap-xs-nowrap css-5u791s&quot;]/div[@class=&quot;MuiCollapse-root MuiCollapse-vertical MuiCollapse-entered css-c4sutr&quot;]/div[@class=&quot;MuiCollapse-wrapper MuiCollapse-vertical css-hboir5&quot;]/div[@class=&quot;MuiCollapse-wrapperInner MuiCollapse-vertical css-8atqhb&quot;]/div[@class=&quot;MuiGrid-root css-42de2x&quot;]/div[@class=&quot;MuiGrid-root css-1ymz3lr&quot;]/button[@class=&quot;MuiButtonBase-root MuiIconButton-root MuiIconButton-sizeMedium css-1jwxc8t&quot;]/span[@class=&quot;material-symbols-outlined MuiBox-root css-11ahsy5&quot;]</value>
      <webElementGuid>b5f7ed52-9ef8-480e-97ab-cbe98bd025b1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div[3]/div[2]/div/div/div/div[2]/button[2]/span</value>
      <webElementGuid>5d0eb131-57a2-4cf1-b0e6-36999b5d96f9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='help'])[1]/following::span[2]</value>
      <webElementGuid>0a7300ce-96f8-47b1-8587-3d295f6de304</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='fullscreen'])[1]/following::span[4]</value>
      <webElementGuid>af1465e2-721f-402a-9f62-e6f6d1834f69</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='​'])[1]/preceding::span[4]</value>
      <webElementGuid>be04a093-1440-424a-9a8a-8306809e8719</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='search'])[1]/preceding::span[5]</value>
      <webElementGuid>ff1d9499-5e2e-4c6c-b54a-9636eb2400a5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='account_circle']/parent::*</value>
      <webElementGuid>a63f4386-7117-4afc-b66d-aa55c101deb4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button[2]/span</value>
      <webElementGuid>37749a9e-8b88-4e65-b27b-2e0c3caddf0a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'account_circle' or . = 'account_circle')]</value>
      <webElementGuid>bbae28b2-72cd-4d95-94dc-d37a39e17ae6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
