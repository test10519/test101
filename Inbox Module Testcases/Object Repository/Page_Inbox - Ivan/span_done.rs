<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_done</name>
   <tag></tag>
   <elementGuidId>1997d0c7-c7b7-40d3-a028-42d34a431567</elementGuidId>
   <imagePath>C:\Users\Ivan Gonzales\Katalon Studio\Inbox Module Testcases\ticketclosed.jpg</imagePath>
   <selectorCollection>
      <entry>
         <key>IMAGE</key>
         <value>C:\Users\Ivan Gonzales\Katalon Studio\Inbox Module Testcases\ticketclosed.jpg</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'done' or . = 'done')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.material-symbols-outlined.MuiBox-root.css-1dvl6i1</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='inbox-header']/div[2]/button/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>IMAGE</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>bbcda641-a215-4b59-89aa-de305d3d2efa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>material-symbols-outlined MuiBox-root css-1dvl6i1</value>
      <webElementGuid>04db85c6-265c-432c-a1b4-e31f0e99d42f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>done</value>
      <webElementGuid>21b56ff0-3ecc-4e46-adcb-fd317c7c6e8d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;inbox-header&quot;)/div[@class=&quot;MuiGrid-root css-3oh7ub&quot;]/button[@class=&quot;MuiButtonBase-root MuiIconButton-root MuiIconButton-sizeMedium css-vet8nc&quot;]/span[@class=&quot;material-symbols-outlined MuiBox-root css-1dvl6i1&quot;]</value>
      <webElementGuid>f1340313-849a-4b25-bcfa-4fb43c45a988</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='inbox-header']/div[2]/button/span</value>
      <webElementGuid>17967529-96f3-45df-94d1-87b245f0a4b4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='expand_more'])[3]/following::span[3]</value>
      <webElementGuid>6e525e6b-8ebe-4c46-bafd-efeb9b246144</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='call_received'])[1]/following::span[5]</value>
      <webElementGuid>75b076e0-1a21-4793-92b2-1dcf1584ddeb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='search'])[2]/preceding::span[4]</value>
      <webElementGuid>e0e757a0-a91f-44bc-84f3-e0ed45642ca0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='expand_more'])[4]/preceding::span[6]</value>
      <webElementGuid>ead7ffdc-2be8-4186-af77-92b3c32d0349</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='done']/parent::*</value>
      <webElementGuid>c4dfcc48-1b8d-4b6c-9653-3bb64c007dcf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div[2]/button/span</value>
      <webElementGuid>00bb9b4d-8d1e-4af3-a7d7-2dd8afe6a93a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'done' or . = 'done')]</value>
      <webElementGuid>3ce36d26-e09a-4627-983a-8d61dabeb70f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
