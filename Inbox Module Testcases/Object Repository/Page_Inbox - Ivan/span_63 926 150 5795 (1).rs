<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_63 926 150 5795 (1)</name>
   <tag></tag>
   <elementGuidId>30941610-42e9-44f8-8c34-5f14a2c39064</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='inbox-header']/div/div/div[2]/span[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.MuiTypography-root.MuiTypography-xSmallRegular.css-19cfj96</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>082d52e8-b53d-48a6-9fdd-59714d1492d4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiTypography-root MuiTypography-xSmallRegular css-19cfj96</value>
      <webElementGuid>37da8f93-484c-49b5-9adb-ca9d490f7943</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>+63 926 150 5795</value>
      <webElementGuid>472b750f-cddf-4806-b438-6ac24b78e05c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;inbox-header&quot;)/div[@class=&quot;MuiGrid-root css-u2a8x6&quot;]/div[@class=&quot;MuiGrid-root tour-contact-info css-7bcfnq&quot;]/div[@class=&quot;MuiGrid-root css-1m444xx&quot;]/span[@class=&quot;MuiTypography-root MuiTypography-xSmallRegular css-19cfj96&quot;]</value>
      <webElementGuid>1dddf177-c712-4031-98f3-87ac54ea8516</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='inbox-header']/div/div/div[2]/span[2]</value>
      <webElementGuid>14ca1317-32e6-4a48-9df1-965e9e7f5028</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='phone'])[1]/following::span[1]</value>
      <webElementGuid>37e740f9-83e6-4d1a-adc1-38e9a66d8d41</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='edit'])[1]/following::span[3]</value>
      <webElementGuid>aebad656-a557-40a6-8969-7c730b2a94a8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='content_copy'])[1]/preceding::span[1]</value>
      <webElementGuid>cfbb4d19-2a63-40a4-8446-a052bae78afd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='call_made'])[1]/preceding::span[3]</value>
      <webElementGuid>9df5b523-2a08-4370-b467-b10fc9ecc94d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/span[2]</value>
      <webElementGuid>ba490f97-f546-4bbf-a843-5a53975f0fa0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = '+63 926 150 5795' or . = '+63 926 150 5795')]</value>
      <webElementGuid>15edfce0-6cc4-4215-84af-48fe9818ee21</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
